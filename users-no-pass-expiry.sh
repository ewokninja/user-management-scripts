#!/bin/bash
echo "List of users on this system with passwords that never expire:"
echo "=============================================================="
echo

for USER in $(grep home /etc/passwd | cut -d':' -f1)
do
  if [ "$(chage -l $USER | grep 'Password expires' | cut -d':' -f2)" == ' never' ]
 then
   echo $USER
 fi
done

